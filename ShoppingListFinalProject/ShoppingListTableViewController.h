//
//  ShoppingListTableViewController.h
//  ShoppingListFinalProject
//
//  Created by Orlando Gotera on 12/16/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ShoppingListTableViewController : UITableViewController
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
}

@end
