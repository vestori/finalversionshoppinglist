//
//  main.m
//  ShoppingListFinalProject
//
//  Created by Orlando Gotera on 12/16/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
