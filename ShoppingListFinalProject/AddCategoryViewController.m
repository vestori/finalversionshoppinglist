//
//  AddCategoryViewController.m
//  ShoppingListFinalProject
//
//  Created by Orlando Gotera on 12/17/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "AddCategoryViewController.h"
#import "AddOrEditShoppingListViewController.h"

@interface AddCategoryViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtCategory;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;

@end

@implementation AddCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    context = appDelegate.persistentContainer.viewContext;
    
    if (self.aCategory){
        
        self.txtCategory.text = [self.aCategory valueForKey:@"categoryName"];
    }
    
    // Background and Label changes <><><><><><><><>
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"projectBG"]];
    self.lblCategory.textColor = [UIColor whiteColor];
    
    [self.btnSave.layer setBorderWidth:3.0];
    [self.btnSave.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [self.btnSave setTitleColor:[UIColor colorWithRed:0.39 green:0.86 blue:0.96 alpha:1.0] forState:UIControlStateNormal];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    AddOrEditShoppingListViewController *updateArray = segue.destinationViewController;
//    [updateArray.categories addObject:self.txtCategory.text];
//}

- (IBAction)saveCategory:(UIButton *)sender {
    
//    if (self.aCategory){
//        [self.aCategory setValue:self.txtCategory.text forKey:@"categoryName"];
//    }else{
//
////        Category *myCategory = [[Category alloc]initWithContext:context];
////
////        [myCategory setValue:self.txtCategory.text forKey:@"categoryName"];
////
////        NSLog(@"My New Category: %@", [myCategory valueForKey:@"categoryName"]);
//    
//    [categories addObject:self.txtCategory.text];
//    
//    // zero out the fields
//    self.txtCategory.text = @"";
//    // commit
//    NSError *error = nil;
//    if(![context save:&error]){
//        NSLog(@"%@ %@", error, [error localizedDescription]);
//    }
//    
    // Dismiss the view
    [self.navigationController popViewControllerAnimated:YES];
  
    
}

@end
